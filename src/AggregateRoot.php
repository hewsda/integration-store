<?php

declare(strict_types=1);

namespace Hewsda\EventSourcing;

abstract class AggregateRoot
{
    /**
     * @var int
     */
    protected $version = 0;

    /**
     * @var array
     */
    protected $recordedEvents = [];

    abstract public function aggregateId(): string;

    public function popRecordedEvents(): array
    {
        $pendingEvents = $this->recordedEvents;

        $this->recordedEvents = [];

        return $pendingEvents;
    }

    protected function publish(AggregateChanged $event)
    {
        ++$this->version;

        $this->recordedEvents[] = $event;

        $this->apply($event);
    }

    // fixMe method should be protected
    public static function reconstituteFromHistory(\Iterator $historyEvents)
    {
        $instance = new static();
        $instance->replay($historyEvents);

        return $instance;
    }

    protected function replay(\Iterator $historyEvents)
    {
        foreach ($historyEvents as $pastEvent) {
            ++$this->version;// fix Me version

            $this->apply($pastEvent);
        }
    }

    protected function apply(AggregateChanged $event)
    {
        $handler = 'when' . implode(array_slice(explode('\\', get_class($event)), -1));

        if (!method_exists($this, $handler)) {
            throw new \RuntimeException('Event method does not exists.');
        }

        $this->{$handler}($event);
    }

    protected function __construct()
    {
    }
}