<?php

declare(strict_types=1);

namespace Hewsda\EventSourcing;

use Prooph\Common\Messaging\DomainEvent;

class AggregateChanged extends DomainEvent
{
    /**
     * @var array
     */
    protected $payload = [];

    public static function occur(string $aggregateId, array $payload = []): self
    {
        return new static($aggregateId, $payload);
    }

    public function aggregateId(): string
    {
        return $this->metadata['aggregate_id'];
    }

    public function payload(): array
    {
        return $this->payload;
    }

    protected function setAggregateId(string $aggregateId): void
    {
        $this->metadata['aggregate_id'] = $aggregateId;
    }

    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * AggregateChanged constructor.
     *
     * @param string $aggregateId
     * @param array $payload
     * @param array $metadata
     */
    protected function __construct(string $aggregateId, array $payload, array $metadata = [])
    {
        $this->metadata = $metadata;
        $this->setAggregateId($aggregateId);
        $this->setPayload($payload);
        $this->init();
    }
}